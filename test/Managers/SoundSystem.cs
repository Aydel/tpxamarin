﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace test.Managers
{
    class SoundSystem
    {
        private MediaPlayer mp = new MediaPlayer();
        public Boolean stopped = false;

        public void AddSong(Context context, int sound)
        {
            if(!stopped)
            {
                if (!mp.IsPlaying)
                {
                    mp = MediaPlayer.Create(context, sound);
                    mp.Start();
                }
                else
                {
                    MediaPlayer media = MediaPlayer.Create(context, sound);
                    mp.SetNextMediaPlayer(media);
                    mp.Start();
                }
            }
        }

        public void changeStatus()
        {
            if(stopped)
            {
                stopped = false;
            }
            else
            {
                stopped = true;
            }
        }

        
    }
}