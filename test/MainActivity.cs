﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using System;
using test.Models;
using Android.Views;
using System.Threading.Tasks;
using Android.Media;
using System.Collections.Generic;
using System.Linq;
using Android.Hardware;
using Android.Content;
using test.Managers;

namespace test
{
    [Activity(Label = "TP Sensors", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        OrientationReader orientationReader = new OrientationReader();
        GyroscopeReader gyroscopeReader = new GyroscopeReader();

        #region debug
        //private static TextView tvAccX;
        //private static TextView tvAccY;
        //private static TextView tvAccZ;

        //private static TextView tvOrX;
        //private static TextView tvOrY;
        //private static TextView tvOrZ;
        //private static TextView tvOrW;

        //private static TextView tvGyrX;
        //private static TextView tvGyrY;
        //private static TextView tvGyrZ;

        //private static TextView tvAction;
        //private static Button btnPlay;

        #endregion

        private static Button btnVoice1;
        private static Button btnVoice2;
        private static Button btnVoiceOff;

        private Dictionary<int, int> usedVoice;

        private Dictionary<int, int> voice1 = new Dictionary<int, int>()
        {
            {1,Resource.Raw.Voice01_01},
            {2,Resource.Raw.Voice01_02},
            {3,Resource.Raw.Voice01_03},
            {4,Resource.Raw.Voice01_04},
            {5,Resource.Raw.Voice01_05},
            {6,Resource.Raw.Voice01_06},
            {7,Resource.Raw.Voice01_07},
            {8,Resource.Raw.Voice01_08},
            {9,Resource.Raw.Voice01_09},
            {10,Resource.Raw.Voice01_10}
        };

        private Dictionary<int, int> voice2 = new Dictionary<int, int>()
        {
            {1,Resource.Raw.Voice02_01},
            {2,Resource.Raw.Voice02_02},
            {3,Resource.Raw.Voice02_03},
            {4,Resource.Raw.Voice02_04},
            {5,Resource.Raw.Voice02_05},
            {6,Resource.Raw.Voice02_06},
            {7,Resource.Raw.Voice02_07},
            {8,Resource.Raw.Voice02_08},
            {9,Resource.Raw.Voice02_09},
            {10,Resource.Raw.Voice02_10}
        };

        SoundSystem ss = new SoundSystem();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            #region debug


            //tvAccX = FindViewById<TextView>(Resource.Id.tvAccX);
            //tvAccY = FindViewById<TextView>(Resource.Id.tvAccY);
            //tvAccZ = FindViewById<TextView>(Resource.Id.tvAccZ);

            //tvOrX = FindViewById<TextView>(Resource.Id.tvOrX);
            //tvOrY = FindViewById<TextView>(Resource.Id.tvOrY);
            //tvOrZ = FindViewById<TextView>(Resource.Id.tvOrZ);
            //tvOrW = FindViewById<TextView>(Resource.Id.tvOrW);

            //tvGyrX = FindViewById<TextView>(Resource.Id.tvGyrX);
            //tvGyrY = FindViewById<TextView>(Resource.Id.tvGyrY);
            //tvGyrZ = FindViewById<TextView>(Resource.Id.tvGyrZ);

            //tvAction = FindViewById<TextView>(Resource.Id.txtAction);


            #endregion

            btnVoice1 = FindViewById<Button>(Resource.Id.btnVoix1);
            btnVoice2 = FindViewById<Button>(Resource.Id.btnVoix2);
            btnVoiceOff = FindViewById<Button>(Resource.Id.btnVoixOff);

            btnVoice1.Click += BtnChooseVoiceOne_Click;
            btnVoice2.Click += BtnChooseVoiceTwo_Click;
            btnVoiceOff.Click += BtnVoiceOff_Click;

            usedVoice = voice1;
            startTimer();
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        private void BtnChooseVoiceOne_Click(object sender, EventArgs e)
        {
            this.usedVoice = voice1;
        }
        private void BtnChooseVoiceTwo_Click(object sender, EventArgs e)
        {
            this.usedVoice = voice2;
        }
        private void BtnVoiceOff_Click(object sender, EventArgs e)
        {
            ss.changeStatus();
        }

        public void startTimer()
        {
            LinearAccelerometer.AccelerometerShockDetected += () =>
            {
                if(LinearAccelerometer.Instance.shockCount == 1)
                {
                    ss.AddSong(this, usedVoice[5]);
                }
                else if(LinearAccelerometer.Instance.shockCount == 2)
                {
                    ss.AddSong(this, usedVoice[6]);
                }
                else if (LinearAccelerometer.Instance.shockCount == 3)
                {
                    ss.AddSong(this, usedVoice[7]);
                    LinearAccelerometer.Instance.shockCount = 0;
                }
            };
            orientationReader.OnInitEvent += (sender, e) =>
            {
                Console.WriteLine("init !");
                ss.AddSong(this, usedVoice[1]);
            };
            orientationReader.OnRollEvent += (sender, e) =>
            {
                Console.WriteLine("ROLL ! Nb°" + orientationReader.rollNumber);
                if (orientationReader.rollNumber == 2)
                {
                    ss.AddSong(this, usedVoice[4]);
                    orientationReader.rollNumber = 0;
                }
                else
                {
                    ss.AddSong(this, usedVoice[2]);
                }
            };
            orientationReader.OnRollEnded += (sender, e) =>
            {
                Console.WriteLine("Roll ended Event !");
            };
            orientationReader.OnLongRollEvent += (sender, e) =>
            {
                Console.WriteLine("Long Roll Event");
                ss.AddSong(this, usedVoice[3]);
            };

            int countUI = 0;
            System.Timers.Timer Timer1 = new System.Timers.Timer();
            Timer1.Start();
            Timer1.Interval = 200;
            Timer1.Enabled = true;
            gyroscopeReader.ToggleGyroscope();
            orientationReader.ToggleOrientationSensor();
            
            if (LinearAccelerometer.Instance.SensorManager == null)
            {
                LinearAccelerometer.Instance.SensorManager = (SensorManager)GetSystemService(Context.SensorService);

                if (LinearAccelerometer.Instance.SensorManager.GetDefaultSensor(SensorType.LinearAcceleration) != null
                    && LinearAccelerometer.Instance.SensorManager.GetDefaultSensor(SensorType.RotationVector) != null)
                {
                    LinearAccelerometer.Instance.StartAccelerometerTracking();

                }
            }
            Timer1.Elapsed += (object sender, System.Timers.ElapsedEventArgs e) =>
            {
                RunOnUiThread(() =>
                {
                    #region debug
                    //if (countUI < 5)
                    //{
                    //    countUI++;

                    //}
                    //else
                    //{
                    //    //tvAccX.Text = accelerometerReader.averageAX.ToString();
                    //    //tvAccY.Text = accelerometerReader.averageAY.ToString();
                    //    //tvAccZ.Text = accelerometerReader.averageAZ.ToString();
                    //    tvAccX.Text = LinearAccelerometer.Instance.CurrentSensorData.X.ToString();
                    //    tvAccY.Text = LinearAccelerometer.Instance.CurrentSensorData.Y.ToString();
                    //    tvAccZ.Text = LinearAccelerometer.Instance.CurrentSensorData.Z.ToString();
                    //    countUI = 0;
                    //}



                    //tvOrX.Text = orientationReader.OrX.ToString();
                    //tvOrY.Text = orientationReader.OrY.ToString();
                    //tvOrZ.Text = orientationReader.OrZ.ToString();
                    //tvOrW.Text = orientationReader.OrW.ToString();

                    //tvGyrX.Text = gyroscopeReader.GyrX.ToString();
                    //tvGyrY.Text = gyroscopeReader.GyrY.ToString();
                    //tvGyrZ.Text = gyroscopeReader.GyrZ.ToString();
                    #endregion
                });
            };
        }
    }
}