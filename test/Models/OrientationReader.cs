﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Essentials;

namespace test.Models
{
    public class OrientationReader
    {
        // Set speed delay for monitoring changes.
        SensorSpeed speed = SensorSpeed.UI;
        public float OrX;
        public float OrY;
        public float OrZ;
        public float OrW;

        public float initialAxisX;
        public float initialAxisY;
        public float initialAxisZ;

        private bool SetInitialValues = false;
        private bool pickedUp = false;
        private bool rolling = false;

        public Boolean sequenceStarted = false;
        public int rollNumber = 0;
        public int schockNumber = 0;

        public long rollTime = 0;

        Stopwatch stopwatch = new Stopwatch();

        public event EventHandler OnRollEvent;
        public event EventHandler OnRollEnded;
        public event EventHandler OnInitEvent;
        public event EventHandler OnLongRollEvent;

        public OrientationReader()
        {
            // Register for reading changes, be sure to unsubscribe when finished
            OrientationSensor.ReadingChanged += OrientationSensor_ReadingChanged;
        }
        void OrientationSensor_ReadingChanged(object sender,
        OrientationSensorChangedEventArgs e)
        {
            var data = e.Reading;
            //Console.WriteLine($"Reading: X: {data.Orientation.X}, Y: {data.Orientation.Y}, Z: { data.Orientation.Z}, W: { data.Orientation.W}");
            OrX = data.Orientation.X;
            OrY = data.Orientation.Y;
            OrZ = data.Orientation.Z;
            OrW = data.Orientation.W;
            // Process Orientation quaternion (X, Y, Z, and W)

            if (stopwatch.ElapsedMilliseconds > 500 && !SetInitialValues)
            {
                initialAxisX = OrX;
                initialAxisY = OrY;
                initialAxisZ = OrZ;
                SetInitialValues = true;
            }
            if (stopwatch.ElapsedMilliseconds > 1000)
            {

                if (!pickedUp && (Math.Abs(initialAxisX - OrX) > 0.3 || Math.Abs(initialAxisY - OrY) > 0.3 || Math.Abs(initialAxisZ - OrZ) > 0.3))
                {
                    OnInitEvent(this, new EventArgs());
                    pickedUp = true;
                }
                if (pickedUp)
                {
                    if (rolling && (Math.Abs(initialAxisX - OrX) < 0.2 && Math.Abs(initialAxisY - OrY) < 0.2 && Math.Abs(initialAxisZ - OrZ) < 0.2))
                    {
                        rolling = false;
                        rollTime = 0;
                        OnRollEnded(this, new EventArgs());
                    }
                    if (!rolling && (Math.Abs(initialAxisX - OrX) > 0.5 || Math.Abs(initialAxisY - OrY) > 0.5 || Math.Abs(initialAxisZ - OrZ) > 0.5))
                    {
                        OnRollEvent(this, new EventArgs());
                        rollNumber++;
                        rolling = true;
                        rollTime = stopwatch.ElapsedMilliseconds;
                    }
                    if (rolling && (stopwatch.ElapsedMilliseconds - rollTime) > 5000)
                    {
                        OnLongRollEvent(this, new EventArgs());
                    }
                }
            }
        }
        public void ToggleOrientationSensor()
        {
            try
            {
                if (OrientationSensor.IsMonitoring)
                    OrientationSensor.Stop();
                else
                {
                    OrientationSensor.Start(speed);
                    stopwatch.Start();
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Feature not supported on device
            }
            catch (Exception ex)
            {
                // Other error has occurred.
            }
        }
    }
}