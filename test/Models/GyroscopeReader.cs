﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Essentials;

namespace test.Models
{
    public class GyroscopeReader
    {
        // Set speed delay for monitoring changes.
        SensorSpeed speed = SensorSpeed.UI;

        public float GyrX;
        public float GyrY;
        public float GyrZ;

        public GyroscopeReader()
        {
            // Register for reading changes.
            Gyroscope.ReadingChanged += Gyroscope_ReadingChanged;
        }
        void Gyroscope_ReadingChanged(object sender, GyroscopeChangedEventArgs e)
        {
            var data = e.Reading;
            // Process Angular Velocity X, Y, and Z reported in rad/s
            //Console.WriteLine($"Reading: X: {data.AngularVelocity.X}, Y: { data.AngularVelocity.Y}, Z: { data.AngularVelocity.Z}");
            GyrX = data.AngularVelocity.X;
            GyrY = data.AngularVelocity.Y;
            GyrZ = data.AngularVelocity.Z;
        }
        public void ToggleGyroscope()
        {
            try
            {
                if (Gyroscope.IsMonitoring)
                    Gyroscope.Stop();
                else
                    Gyroscope.Start(speed);
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Feature not supported on device
            }
            catch (Exception ex)
            {
                // Other error has occurred.
            }
        }
    }
}