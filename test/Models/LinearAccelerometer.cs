﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Hardware;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace test.Models
{
        public delegate void AccelerometerDataChangedHandler();
        public delegate void AccelerometerShockDetectedHandler();

    public class LinearAccelerometer : Java.Lang.Object, ISensorEventListener
    {
        static readonly object _syncLock = new object();

        public SensorManager SensorManager { get; set; }
        public AccelerometerDataViewModel previousSensorData{ get; private set; }
        public AccelerometerDataViewModel CurrentSensorData { get; private set; }

        public static event AccelerometerDataChangedHandler AccelerometerDataChanged;

        public static event AccelerometerShockDetectedHandler AccelerometerShockDetected;

        #region Singletone

        private static LinearAccelerometer instance;

        Stopwatch stopwatch = new Stopwatch();
        long lastShock = 0;
        public int shockCount = 0;

        private LinearAccelerometer()
        {
        }

        public static LinearAccelerometer Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LinearAccelerometer();
                }
                return instance;
            }
        }

        #endregion

        #region Public Methods

        public void StartAccelerometerTracking()
        {
            currentInterval = -1;

            SensorManager.RegisterListener(this, SensorManager.GetDefaultSensor(SensorType.LinearAcceleration), SensorDelay.Normal);
            stopwatch.Start();
        }

        public void StopAccelerometerTracking()
        {
            if (SensorManager != null)
            {
                SensorManager.UnregisterListener(this);
            }
            stopwatch.Stop();
        }

        #endregion

        #region ISensorEventListener Implementation

        public void OnAccuracyChanged(Sensor sensor, SensorStatus accuracy)
        {
            // We don't want to do anything here.
        }

        public void OnSensorChanged(SensorEvent e)
        {
            lock (_syncLock)
            {
                var model = ToAccelerometerDataViewModel(e);

                if (CurrentSensorData != null)
                {
                    if (currentInterval != DateTime.UtcNow.Second)
                    {
                        currentInterval = DateTime.UtcNow.Second;

                        if (packetData.Count > 0)
                        {
                            packetData = new List<AccelerometerDataViewModel>();
                        }
                    }
                    else
                    {
                        packetData.Add(model);
                    }
                }
                else
                {
                    currentInterval = DateTime.UtcNow.Second;
                    packetData.Add(model);
                }

                if (getAccDiff(model, CurrentSensorData) > 10 && (stopwatch.ElapsedMilliseconds - lastShock) > 1000)
                {
                    lastShock = stopwatch.ElapsedMilliseconds;
                    shockCount++;
                    AccelerometerShockDetected?.Invoke();
                }
                
                CurrentSensorData = model;

                AccelerometerDataChanged?.Invoke();
            }
        }

        private float getAccDiff(AccelerometerDataViewModel modelA, AccelerometerDataViewModel modelB)
        {
            float modelAValue;
            float modelBValue;
            if (null != modelA && null != modelB)
            {
                modelAValue = modelA.X + modelA.Y + modelA.Z;
                modelBValue = modelB.X + modelB.Y + modelB.Z;
                return Math.Abs(modelAValue - modelBValue);
            }

            return -1;
        }

        #endregion

        int currentInterval = -1;

        List<AccelerometerDataViewModel> packetData = new List<AccelerometerDataViewModel>();

        public static AccelerometerDataViewModel ToAccelerometerDataViewModel(SensorEvent value)
        {
            var model = new AccelerometerDataViewModel();

            model.X = Convert.ToSingle(Math.Round(value.Values[0], 2));
            model.Y = Convert.ToSingle(Math.Round(value.Values[1], 2));
            model.Z = Convert.ToSingle(Math.Round(value.Values[2], 2));

            model.Time = DateTime.UtcNow;

            return model;
        }
    }
}